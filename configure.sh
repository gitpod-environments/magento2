url=$(gp url | awk -F"//" {'print $2'}) && url+="/" && url="https://8002-"$url;

cd /workspace/m2dev-slim && composer install && php bin/magento setup:install --db-name='pgannexdigital_nemanja' --db-user='pgannexdigital_root' --db-password='nem4540nem4540' --base-url=$url --backend-frontname='admin' --admin-user='admin' --admin-password='7ezdyof2' --admin-email='vini@annexdigital.ie' --admin-firstname='Vini' --admin-lastname='Siqueira' --use-rewrites='1' --use-secure='1' --base-url-secure=$url --use-secure-admin='1' --language='en_US' --db-host='81.19.211.201' --cleanup-database --timezone='Europe/Dublin' --currency='EUR'

n98-magerun2 module:disable Magento_Csp &&
n98-magerun2 module:disable Magento_TwoFactorAuth &&
n98-magerun2 setup:upgrade &&

php bin/magento config:set web/cookie/cookie_path "/" --lock-config &&
php bin/magento config:set web/cookie/cookie_domain ".gitpod.io" --lock-config &&

bin/magento setup:perf:generate-fixtures setup/performance-toolkit/profiles/ce/small.xml 

n98-magerun2 cache:clean &&
n98-magerun2 cache:flush &&