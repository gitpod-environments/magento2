#!/bin/bash

$ES_HOME78/bin/elasticsearch -d -p $ES_HOME78/pid -E discovery.type=single-node &

url=$(gp url | awk -F"//" {'print $2'}) && url+="/" && url="https://8002-"$url;

bin/magento config:set web/unsecure/base_url $url
bin/magento config:set web/secure/base_url $url

n98-magerun2 cache:clean && n98-magerun2 cache:flush