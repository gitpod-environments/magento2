#!/bin/bash
cd /workspace/magento2 &&
composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition=2.4.2 m2 -vvv
cd m2 && cp -avr .* /workspace/magento2;
cd /workspace/magento2 && rm -r -f m2;